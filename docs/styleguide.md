*«Пишите код так, как будто сопровождать его будет склонный к насилию психопат, который знает, где вы живёте»*

За основу взят [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)

[Наглядные примеры с комментариями](/docs/styleguide/examples.hpp)

### Линтеры

Стайлгайд является обязательным, его соблюдение проверяется автоматически.

Для автоматической проверки и коррекции стиля мы используем [clang-format](https://clang.llvm.org/docs/ClangFormat.html) и [clang-tidy](http://clang.llvm.org/extra/clang-tidy/)

Конфиги линтеров лежат в корне репозитория: [.clang-format](/.clang-format) и [.clang-tidy](/.clang-tidy)
